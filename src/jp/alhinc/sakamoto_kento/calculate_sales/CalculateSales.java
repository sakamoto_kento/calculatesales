package jp.alhinc.sakamoto_kento.calculate_sales;



import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class CalculateSales {
	public static void main(String[] args) {

		//KEY[支店コード] VALUE[支店名]
		Map<String, String> branchmap = new HashMap<>();
		//KEY[支店コード] VALUE[売上金額]
		Map<String, Long> salesmap = new HashMap<>();


		BufferedReader br = null;
		try {
			File branchfile = new File(args[0], "branch.lst");
			//支店定義ファイルが存在しない場合
			if (!branchfile.exists()) {
				System.out.print("支店定義ファイルが存在しません");
				return;
			}
			//支店定義ファイル読み込み
			FileReader fr = new FileReader(branchfile);
			br = new BufferedReader(fr);
			//定義ファイル抽出
			String branchline;
			while ((branchline = br.readLine()) != null) {
				String[] branchdate = branchline.split("\\,");
				// MAPにデータを格納
				branchmap.put(branchdate[0], branchdate[1]);
				salesmap.put(branchdate[0], (long) 0);

				//支店定義ファイルのフォーマット
				//数字3桁か判定
				if (!branchdate[0].matches("^[0-9]{3}$")) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				if (branchdate.length != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}
		//売上ファイルの一覧
		File foldpath = new File(args[0]);
		File[] salesfile = foldpath.listFiles();
		// 売上ファイル名リスト
		ArrayList<Integer> filelist = new ArrayList<Integer>();
		for (int i = 0; i < salesfile.length; i++) {
			//数字8桁かつ拡張子が.rcdでファイルデータのみを絞込（フォルダは除外）
			if (salesfile[i].isFile() && salesfile[i].getName().matches("^[0-9]{8}\\.rcd$")) {
				String filename[] = salesfile[i].getName().split("\\.");
				//ファイル名をリストに追加
				filelist.add(Integer.parseInt(filename[0]));
				// 売上ファイルデータリスト
				List<String> salesdate = new ArrayList<>();

				//売上ファイル読み込み
				try {
					FileReader fileReader = new FileReader(salesfile[i]);
					BufferedReader fr = new BufferedReader(fileReader);
					br = new BufferedReader(fr);
					//売上ファイルの支店コード、金額を抽出
					String date;
					while ((date = br.readLine()) != null) {
					//売上ファイルのデータをリストに追加
					salesdate.add(date);
					}
					//支店に該当がなかった場合
					if (!salesmap.containsKey(salesdate.get(0))) {
						System.out.println(salesfile[i].getName() + "の支店コードが不正です");
						return;
					}
					//売上額の加算
					long salessum = salesmap.get(salesdate.get(0)) + Long.parseLong(salesdate.get(1));
					//合計金額が10桁超えた場合
					if (String.valueOf(salessum).length() > 10) {
						System.out.println("合計金額が10桁を超えました");
						return;
					}
					//salesmapに支店番号、合計売上金額格納
					salesmap.put(salesdate.get(0), salessum);
					//売上ファイルの中身が２行以外の場合
					int line = salesdate.size();
					if (line != 2) {
						System.out.println(salesfile[i].getName() + "のフォーマットが不正です");
						return;
					}
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				} finally {
					if (br != null) {
						try {
							br.close();
						} catch (IOException e) {
							System.out.println("予期せぬエラーが発生しました");
							return;
						}
					}
				}
			}
			//売上ファイルの歯抜けチェック
			for (int j = 0; j < filelist.size() - 1; j++) {
				if (filelist.get(j + 1) - filelist.get(j) != 1) {
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}
			}
			//集計ファイル出力
			BufferedWriter bw = null;
			try {
				File outfile = new File(args[0], "branch.out");
				FileWriter fw = new FileWriter(outfile);
				bw = new BufferedWriter(fw);
				//書き込み
				for (Entry<String, Long> k : salesmap.entrySet()) {
					bw.write(k.getKey() + "," + branchmap.get(k.getKey()) + "," + salesmap.get(k.getKey()) + ",");
					bw.newLine();
				}
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}
	}
}
